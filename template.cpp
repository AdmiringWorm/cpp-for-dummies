///
//  Template - provides a template to be used as the
//             starting point
//
// the following include files define the majority of
// the functions that any given program will need
#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

int main(int nNumberofArgs, char *pszArgs[])
{
  (void)nNumberofArgs;
  (void)pszArgs;
  // your C++ code starts here

  // wait until user is ready before terminating program
  // to allow the user to see the program results
  cout << "Press Enter to continue..." << endl;
  cin.ignore(10, '\n');
  cin.get();
  return 0;
}
