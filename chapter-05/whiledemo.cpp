///
//  WhileDemo - input a loop count. Loop while
//              outputting a string arg number of times.
//
// the following include files define the majority of
// the functions that any given program will need
#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

int main(int nNumberofArgs, char *pszArgs[])
{
  (void)nNumberofArgs;
  (void)pszArgs;

  // input the loop count
  int nLoopCount;
  cout << "Enter loop count: ";
  cin  >> nLoopCount;

  // now loop that many times
  while (nLoopCount > 0) {
    nLoopCount = nLoopCount - 1;
    cout << "Only " << nLoopCount
         << " loops to go" << endl;
  }

  // wait until user is ready before terminating program
  // to allow the user to see the program results
  cout << "Press Enter to continue..." << endl;
  cin.ignore(10, '\n');
  cin.get();
  return 0;
}
