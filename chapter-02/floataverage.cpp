//
//  FloatAverage - average 3 numbers using floating point arithmetic.
//                 Otherwise, same as IntAverage
//
#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

int main(int nNumberofArgs, char *pszArgs[])
{
  (void)nNumberofArgs;
  (void)pszArgs;

  float fValue1;
  float fValue2;
  float fValue3;

  // enter three numbers
  cout << "This program averages three numbers using "
       << "floating point arithmetic" << endl << endl;
  cout << "Enter three integers:" << endl;

  cout << "f1 - ";
  cin >> fValue1;

  cout << "f2 - ";
  cin >> fValue2;

  cout << "f3 - ";
  cin >> fValue3;

  // first the sum of three ratios
  cout << "f1/3 + f2/3 + f3/3 = ";
  cout << fValue1 / 3 + fValue2 / 3 + fValue3 / 3;
  cout << endl;

  // now the ratio of three sums
  cout << "(f1 + f2 + f3)/3   = ";
  cout << (fValue1 + fValue2 + fValue3) / 3;
  cout << endl;

  // wait until user is ready before terminating program
  // to allow the user to see the program results
  cout << "Press Enter to continue..." << endl;
  cin.ignore(10, '\n');
  cin.get();
  return 0;
}
